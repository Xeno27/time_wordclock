#include "jsontest.h"
#include "cJSON.h"

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* return 1 if the monitor supports full hd, 0 otherwise */
int supports_full_hd(const char * const monitor)
{
    const cJSON *resolution = NULL;
    const cJSON *resolutions = NULL;
    const cJSON *name = NULL;
    int status = 0;
    cJSON *monitor_json = cJSON_Parse(monitor);
    if (monitor_json == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

    name = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
    if (cJSON_IsString(name) && (name->valuestring != NULL))
    {
        printf("Checking monitor \"%s\"\n", name->valuestring);
    }

    resolutions = cJSON_GetObjectItemCaseSensitive(monitor_json, "resolutions");
    cJSON_ArrayForEach(resolution, resolutions)
    {
        cJSON *width = cJSON_GetObjectItemCaseSensitive(resolution, "width");
        cJSON *height = cJSON_GetObjectItemCaseSensitive(resolution, "height");

        if (!cJSON_IsNumber(width) || !cJSON_IsNumber(height))
        {
            status = 0;
            printf("0 \r\n");
            goto end;
        }

        if ((width->valuedouble == 1920) && (height->valuedouble == 1080))
        {
            status = 1;
            printf("1 \r\n");
            goto end;
        }
    }

end:
    cJSON_Delete(monitor_json);
    return status;
}

void jsontest(){
    /*
     * windows need double \\
     * C:\\git\\time_wordclock\\JSON\\time_generator_test.json
     */
    FILE *f = fopen("C:\\git\\time_wordclock\\JSON\\cjson_test.json", "rb");
    if (f == NULL){
        printf("error filepointer is NULL \r\n");
    }
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *f_r_string = malloc(fsize + 1);
    fread(f_r_string, 1, fsize, f);
    fclose(f);

    f_r_string[fsize] = 0;
    //sting_const = f_r_string;
    printf("lengh : %ld \r\n", fsize);
    printf("json string -> %s \r\n",f_r_string);

    supports_full_hd(f_r_string);
}
