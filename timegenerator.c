/*
 * Christoph Metzner
 * Frame Generation for wordclock mk III
 * christoph-metzner@t-online.de
 *
 *
 */

#include "timegenerator.h"
#include <string.h>
#include <strings.h>
//#include <QDebug>

/*
 * adjustments
 */


/*
 * primary pixel color and frame
 */
static struct pixel primarry_pixel_frame[PIXEL_COUNT_PRIMARY];
static struct pixel primary_pixel_color;
static struct pixel primary_pixel_color_h;
static struct pixel primary_pixel_color_m;
static struct pixel primary_pixel_color_empty;

/*
 * secondary pixel and color frame (minutes and e.g temperature)
 */
static struct pixel secondary_pixel_frame[PIXEL_COUNT_SECONDARY];
static struct pixel secondary_pixel_color;

static struct time_Hour_12h time_hour;
static struct time_5M time_min;

//static const char *sting_const;

/*
 * DEBUG
 *
 */
#define DEBUG 1

/*
 * Generates from time frame
 *
 */
void timegen(int h, int m){

    /*
     * switch in hour
     */
#ifdef DEBUG
    printf("h..\r\n");
#endif
    switch(h){
    case 1:
#ifdef DEBUG
        printf("1 \r\n");
#endif
        enter_pixel_array(time_hour.one,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 2:
#ifdef DEBUG
        printf("2 \r\n");
#endif
        enter_pixel_array(time_hour.two,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 3:
#ifdef DEBUG
        printf("3 \r\n");
#endif
        enter_pixel_array(time_hour.tree,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 4:
#ifdef DEBUG
        printf("4 \r\n");
#endif
        enter_pixel_array(time_hour.four,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 5:
#ifdef DEBUG
        printf("5 \r\n");
#endif
        enter_pixel_array(time_hour.five,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 6:
#ifdef DEBUG
        printf("6 \r\n");
#endif
        enter_pixel_array(time_hour.six,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 7:
#ifdef DEBUG
        printf("7 \r\n");
#endif
        enter_pixel_array(time_hour.seven,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 8:
#ifdef DEBUG
        printf("8 \r\n");
#endif
        enter_pixel_array(time_hour.eight,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 9:
#ifdef DEBUG
        printf("9 \r\n");
#endif
        enter_pixel_array(time_hour.nine,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 10:
#ifdef DEBUG
        printf("10 \r\n");
#endif
        enter_pixel_array(time_hour.ten,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 11:
#ifdef DEBUG
        printf("11 \r\n");
#endif
        enter_pixel_array(time_hour.eleven,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    case 12:
#ifdef DEBUG
        printf("12 \r\n");
#endif
        enter_pixel_array(time_hour.twelve,LIST_LENGH_H,primary_pixel_color,primarry_pixel_frame);
        break;
    }

    /*
     * switch in minutes
     * have to correct
     */
#ifdef DEBUG
    printf("m..\r\n");
#endif
    switch (m) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
#ifdef DEBUG
        printf("0..4 \r\n");
#endif
        /*
         * just display minutes via rect led´s
         */
        break;
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
#ifdef DEBUG
        printf("5..9 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_5,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
#ifdef DEBUG
        printf("10..14 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_10,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
#ifdef DEBUG
        printf("15..19 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_15,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
#ifdef DEBUG
        printf("20..24 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_20,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
#ifdef DEBUG
        printf("25..29 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_25,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 30:
    case 31:
    case 32:
    case 33:
    case 34:
#ifdef DEBUG
        printf("30..34 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_30,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 35:
    case 36:
    case 37:
    case 38:
    case 39:
#ifdef DEBUG
        printf("35..39 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_35,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 40:
    case 41:
    case 42:
    case 43:
    case 44:
#ifdef DEBUG
        printf("40..44 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_40,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 45:
    case 46:
    case 47:
    case 48:
    case 49:
#ifdef DEBUG
        printf("45..49 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_45,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 50:
    case 51:
    case 52:
    case 53:
    case 54:
#ifdef DEBUG
        printf("50..54 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_50,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    case 55:
    case 56:
    case 57:
    case 58:
    case 59:
#ifdef DEBUG
        printf("55..59 \r\n");
#endif
        enter_pixel_array(time_min.base_plus_55,LIST_LENGH_M,primary_pixel_color,primarry_pixel_frame);
        break;
    }

    /*
     * have to correct
     */
    int einer = m % 10;
    /*
     * 0 = 0 and 5 = 0 !
     */
    if ((!(einer == 0)) && (!(einer == 5))){
#ifdef DEBUG
        printf("secondary...  \r\n");
#endif
        if (einer <= 5){
            int i;
            for(i = 0; i < (einer); i++){
                enter_singel_pixel(i,secondary_pixel_color,secondary_pixel_frame);
            }
        }else{

            int i;
            for(i = 0; i < (einer -5); i++){
                enter_singel_pixel(i,secondary_pixel_color,secondary_pixel_frame);
            }
        }

    }

};


void enter_singel_pixel(int nr,struct pixel pix,struct pixel pixel_frame[]){

    if(!((pix.R == 0) && (pix.G == 0) && (pix.B == 0))){
#ifdef DEBUG
        printf("nr: %d ->",nr);
#endif
        pixel_frame[nr].R = pix.R;
#ifdef DEBUG
        printf("R:%d,",pixel_frame[nr].R);
#endif
        pixel_frame[nr].G = pix.G;
#ifdef DEBUG
        printf("G:%d,",pixel_frame[nr].G);
#endif
        pixel_frame[nr].B = pix.B;
#ifdef DEBUG
        printf("B:%d \r\n",pixel_frame[nr].B);
#endif
    }
    else {
        printf("error RBG empty \r\n");
    }

}


void enter_pixel_array(int array[], int n_Anzahl, struct pixel pix, struct pixel pixel_frame[]){
    int i;
    for(i = 0; i < n_Anzahl; i++){
        if (array[i] == -1){
#ifdef DEBUG
            printf("no value  break\r\n");
#endif
            break;
        }else{
             enter_singel_pixel(array[i],pix,pixel_frame);
        }

    }
}

/**
 * @brief init_array
 * @param array
 * @param n_Anzahl
 */
void init_time_array(int array[], int n_Anzahl){
    int i;
    for(i = 0; i < n_Anzahl; i++){
        array[i] = -1;
    }
}

void init_frame(struct pixel pixel_frame[], int n_Anzahl){
    int i;
    for(i = 0; i < n_Anzahl; i++){
        pixel_frame[i].R = 0;
        pixel_frame[i].G = 0;
        pixel_frame[i].B = 0;
    }
}

/**
 * @brief init_timegen
 */
void init_timegen(){

    /*  init time gen ..
     * init all arrays with default values
     *
     *
     */

    /*
     * init Hour
     */
    init_time_array(time_hour.one,LIST_LENGH_H );
    init_time_array(time_hour.two,LIST_LENGH_H );
    init_time_array(time_hour.tree,LIST_LENGH_H );
    init_time_array(time_hour.four,LIST_LENGH_H );
    init_time_array(time_hour.five,LIST_LENGH_H );
    init_time_array(time_hour.six,LIST_LENGH_H );
    init_time_array(time_hour.seven,LIST_LENGH_H );
    init_time_array(time_hour.eight,LIST_LENGH_H );
    init_time_array(time_hour.nine,LIST_LENGH_H );
    init_time_array(time_hour.ten,LIST_LENGH_H );
    init_time_array(time_hour.eleven,LIST_LENGH_H );
    init_time_array(time_hour.twelve,LIST_LENGH_H );

    /*
     * init Minute
     */
    init_time_array(time_min.base_plus_5,LIST_LENGH_M );
    init_time_array(time_min.base_plus_10,LIST_LENGH_M );
    init_time_array(time_min.base_plus_15,LIST_LENGH_M );
    init_time_array(time_min.base_plus_20,LIST_LENGH_M );
    init_time_array(time_min.base_plus_25,LIST_LENGH_M );
    init_time_array(time_min.base_plus_30,LIST_LENGH_M );
    init_time_array(time_min.base_plus_35,LIST_LENGH_M );
    init_time_array(time_min.base_plus_40,LIST_LENGH_M );
    init_time_array(time_min.base_plus_45,LIST_LENGH_M );
    init_time_array(time_min.base_plus_50,LIST_LENGH_M );
    init_time_array(time_min.base_plus_55,LIST_LENGH_M );

    /*
     * color
     */
    primary_pixel_color.R = 0;
    primary_pixel_color.G = 0;
    primary_pixel_color.B = 0;

    secondary_pixel_color.R = 0;
    secondary_pixel_color.G = 0;
    secondary_pixel_color.B = 0;

    /*
     * init frame buffer
     */
    init_frame(primarry_pixel_frame,PIXEL_COUNT_PRIMARY);
    init_frame(secondary_pixel_frame,PIXEL_COUNT_SECONDARY);
}

void debug_array(int array[], int n_Anzahl){
    int i;
    for(i = 0; i < n_Anzahl; i++){
        printf("c: %d , value : %d \r\n",i,array[i]);
    }
}

void debug_pixelframe(struct pixel pixel_frame[], int n_Anzahl){
    int i;
    for(i = 0; i < n_Anzahl; i++){
        printf("NR: %d , R%d, G%d, B%d \r\n",i,pixel_frame[i].R,pixel_frame[i].G,pixel_frame[i].B);
    }
}




void test_json(){
    printf("test json \r\n");
    /*
     * windows need double \\
     * C:\\git\\time_wordclock\\JSON\\time_generator_test.json
     */
    FILE *f = fopen("C:\\git\\time_wordclock\\JSON\\time_generator_test.json", "rb");
    //FILE *f = fopen("C:\\git\\time_wordclock\\JSON\\cjson_test.json", "rb");
    if (f == NULL){
        printf("error filepointer is NULL \r\n");
    }
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *f_r_string = malloc(fsize + 1);
    fread(f_r_string, 1, fsize, f);
    fclose(f);

    f_r_string[fsize] = 0;
    printf("lengh : %ld \r\n", fsize);
    printf("json string -> %s \r\n",f_r_string);

    read_in_json_config(f_r_string);
    printf("one \r\n");
    debug_array(time_hour.one,LIST_LENGH_H);
    printf("two \r\n");
    debug_array(time_hour.two,LIST_LENGH_H);
    printf("tree \r\n");
    debug_array(time_hour.tree,LIST_LENGH_H);

    printf("5\r\n");
    debug_array(time_min.base_plus_5,LIST_LENGH_M);
    printf("10 \r\n");
    debug_array(time_min.base_plus_10,LIST_LENGH_M);
    printf("15 \r\n");
    debug_array(time_min.base_plus_15,LIST_LENGH_M);


}
void read_json_to_array(int int_array[], cJSON *jsonObject){
    cJSON *tmp = NULL;
    int n_Anzahl = cJSON_GetArraySize(jsonObject);
    printf("size %d \r\n",n_Anzahl);

    if (cJSON_IsArray(jsonObject)){
        int i;
        for(i = 0; i < n_Anzahl; i++){
             cJSON *T = cJSON_GetArrayItem(jsonObject,i);
#ifdef DEBUG
             printf("c %d , value : %d \r\n",i,T->valueint);
#endif
             int_array[i] = T->valueint;

        }


    }


}

void openjson(const char *filepath){
    /*
     * windows need double \\
     * C:\\git\\time_wordclock\\JSON\\time_generator_test.json
     */
    FILE *f = fopen(filepath, "rb");
    if (f == NULL){
        printf("error filepointer is NULL \r\n");
    }
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *f_r_string = malloc(fsize + 1);
    fread(f_r_string, 1, fsize, f);
    fclose(f);

    f_r_string[fsize] = 0;

#ifdef DEBUG
    printf("lengh : %ld \r\n", fsize);
    printf("json string -> %s \r\n",f_r_string);
#endif

    /*
     * read in json config
     */
    read_in_json_config(f_r_string);
}

void read_in_json_config(const char * const json_input)
{
        #ifdef DEBUG
        printf("json input string -> %s \r\n",json_input);
#endif
        const cJSON *j_time_h = NULL;
        const cJSON *j_time_m = NULL;
        const cJSON *j_color_m = NULL;
        const cJSON *j_color_h = NULL;
        const cJSON *j_color_empty = NULL;

        int status = 0;
        cJSON *json_parse_output = cJSON_Parse(json_input);
        if (json_parse_output == NULL)
        {
            printf("error json file == NULL \r\n");
        }else{
#ifdef DEBUG
            printf("pars ok \r\n");
#endif
        }
        /*
         * Read in time hour
         */
        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_1");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.one,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_2");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.two,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_3");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.tree,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_4");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.four,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_5");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.five,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_6");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.six,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_7");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.seven,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_8");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.eight,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_9");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.nine,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_10");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.ten,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_11");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.eleven,j_time_h);
        }

        j_time_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_h_12");
        if(cJSON_IsArray(j_time_h)){
            read_json_to_array(time_hour.twelve,j_time_h);
        }

        /*
         * Read in time minute
         */
        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_5");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_5,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_10");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_10,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_15");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_15,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_20");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_20,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_25");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_25,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_30");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_30,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_35");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_35,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_40");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_40,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_45");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_45,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_50");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_50,j_time_m);
        }

        j_time_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "time_m_55");
        if(cJSON_IsArray(j_time_m)){
            read_json_to_array(time_min.base_plus_55,j_time_m);
        }

        /*
         * pixel color
         */
        j_color_h = cJSON_GetObjectItemCaseSensitive(json_parse_output, "color_h");
        if(j_color_h != NULL){

            cJSON *tmp = cJSON_GetObjectItemCaseSensitive(j_color_h,"R");
            primary_pixel_color_h.R = tmp->valueint;
            printf("color R %d \r\n",primary_pixel_color_h.R);

            tmp = cJSON_GetObjectItemCaseSensitive(j_color_h,"G");
            primary_pixel_color_h.G = tmp->valueint;

            tmp = cJSON_GetObjectItemCaseSensitive(j_color_h,"B");
            primary_pixel_color_h.B = tmp->valueint;
        }

        j_color_m = cJSON_GetObjectItemCaseSensitive(json_parse_output, "color_m");
        if(j_color_m != NULL){

            cJSON *tmp = cJSON_GetObjectItemCaseSensitive(j_color_m,"R");
            primary_pixel_color_m.R = tmp->valueint;

            tmp = cJSON_GetObjectItemCaseSensitive(j_color_m,"G");
            primary_pixel_color_m.G = tmp->valueint;

            tmp = cJSON_GetObjectItemCaseSensitive(j_color_m,"B");
            primary_pixel_color_m.B = tmp->valueint;
        }

        j_color_empty = cJSON_GetObjectItemCaseSensitive(json_parse_output, "color_empty");
        if(j_color_empty != NULL){

            cJSON *tmp = cJSON_GetObjectItemCaseSensitive(j_color_empty,"R");
            primary_pixel_color_empty.R = tmp->valueint;

            tmp = cJSON_GetObjectItemCaseSensitive(j_color_empty,"G");
            primary_pixel_color_empty.G = tmp->valueint;

            tmp = cJSON_GetObjectItemCaseSensitive(j_color_empty,"B");
            primary_pixel_color_empty.B = tmp->valueint;
        }

}

void test_time(){
    init_timegen();
#ifdef DEBUG
    printf("hallo from timegen \r\n");
#endif

    printf("open json ... \r\n");
    openjson("C:\\git\\time_wordclock\\JSON\\time_generator_test.json");

    primary_pixel_color = primary_pixel_color_h;
    secondary_pixel_color = primary_pixel_color_m;

    printf("1,6: \r\n");
    timegen(1,6);
    printf("debug primary \r\n");
    debug_pixelframe(primarry_pixel_frame,PIXEL_COUNT_PRIMARY);
    printf("debug secondary \r\n");
    debug_pixelframe(secondary_pixel_frame,PIXEL_COUNT_SECONDARY);

        debug_array(time_hour.one,LIST_LENGH_H);


    /*
     * init frame buffer
     */
    init_frame(primarry_pixel_frame,PIXEL_COUNT_PRIMARY);
    init_frame(secondary_pixel_frame,PIXEL_COUNT_SECONDARY);


    printf("2,2 \r\n");
    timegen(2,8);

    printf("debug primary \r\n");
    debug_pixelframe(primarry_pixel_frame,PIXEL_COUNT_PRIMARY);
    printf("debug secondary \r\n");
    debug_pixelframe(secondary_pixel_frame,PIXEL_COUNT_SECONDARY);
}
