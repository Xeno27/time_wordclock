#ifndef TIMEGENERATOR_H
#define TIMEGENERATOR_H

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "cJSON.h"

#define H12
#define LIST_LENGH_H 9 // 10 0..9
#define LIST_LENGH_M 9 // 10 0..9
#define PIXEL_COUNT_PRIMARY 110
#define PIXEL_COUNT_SECONDARY 4

#ifdef H12
struct time_Hour_12h {
    int one[LIST_LENGH_H];
    int two[LIST_LENGH_H];
    int tree[LIST_LENGH_H];
    int four [LIST_LENGH_H];
    int five[LIST_LENGH_H];
    int six[LIST_LENGH_H];
    int seven[LIST_LENGH_H];
    int eight[LIST_LENGH_H];
    int nine[LIST_LENGH_H];
    int ten[LIST_LENGH_H];
    int eleven[LIST_LENGH_H];
    int twelve[LIST_LENGH_H];

};
#endif

struct time_5M{
    int base_plus_5[LIST_LENGH_M];
    int base_plus_10[LIST_LENGH_M];
    int base_plus_15[LIST_LENGH_M];
    int base_plus_20[LIST_LENGH_M];
    int base_plus_25[LIST_LENGH_M];
    int base_plus_30[LIST_LENGH_M];
    int base_plus_35[LIST_LENGH_M];
    int base_plus_40[LIST_LENGH_M];
    int base_plus_45[LIST_LENGH_M];
    int base_plus_50[LIST_LENGH_M];
    int base_plus_55[LIST_LENGH_M];
};

struct pixel{
    uint8_t R;
    uint8_t G;
    uint8_t B;
};



void enter_singel_pixel(int nr,struct pixel pix,struct pixel pixel_frame[]);
void enter_pixel_array(int array[], int n_Anzahl, struct pixel pix, struct pixel pixel_frame[]);
void read_in_json_config(const char * const json_input);
void test_time();
void test_json();

#endif // TIMEGENERATOR_H
