TEMPLATE = app
CONFIG += console
#CONFIG -= app_bundle
#CONFIG -= qt


QT       += core gui network

SOURCES += \
        main.c \
    cJSON.c \
    timegenerator.c \
    jsontest.c

HEADERS += \
    timegenerator.h \
    cJSON.h \
    jsontest.h
